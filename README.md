# Theodul Pass Player Download Plugin

A plugin for the new Matterhorn Engage Theodul Player.
Lists the available the video and audio sources in a tab.

Copyright 2015 Henning Strüber, Denis Meyer, License: Educational Community License v2.0

![Screenshot](img/screenshot.png "Screenshot")

# Installation

To add the plugin to your existing theodul installation, just download the compiled plugin jar file (under "Downloads") and copy it into your Matterhorn lib folder.

# Compilation

mvn clean install -DdeployTo=/your/path/to/matterhorn
